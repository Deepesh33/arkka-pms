from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Project(models.Model):
    pj_id = models.IntegerField(default=0,primary_key=True)
    project_name = models.CharField(max_length=300)
    project_location = models.CharField(max_length=300)
    delivery_date = models.DateField
    def __str__(self):
        return self.project_name

class Equipment(models.Model):
    equipment_id = models.AutoField(primary_key='true')
    equipment_name = models.CharField(max_length=300, default='')
    eq_pur_price_exc_tax = models.IntegerField(default=0)       #Unit purchase price excluding tax
    eq_desc = models.CharField(max_length=300)
    equipment_provider = models.CharField(max_length=400,default='')
    eq_sell_price = models.IntegerField(default=0)              #Unit selling price
    eq_notes = models.CharField(max_length=500, default='')
    eq_image = models.ImageField(upload_to='core/static/images/eq_image',default='')

    def __str__(self):
        return self.equipment_name

class AvBudget(models.Model):
    localisation = models.CharField(max_length=300)
    devices = models.CharField(max_length=300)
    provider = models.CharField(max_length=300)
    qty = models.IntegerField(default=0)
    eq_pur_price_exc_tax = models.IntegerField(default=0)       #Unit purchase price excluding tax
    total_pur_price = models.IntegerField(default=0)            #Total purchase price
    eq_sell_price_vat_ex = models.IntegerField(default=0)       #Unit selling price
    total_sell_price_vat_ex = models.IntegerField(default=0)    #Total selling price VAT ex
    sell_price_without_install = models.IntegerField(default=0) #Selling price without installation
    sell_price_in_per = models.IntegerField(default=0)          #Selling price in  %
    install_cost_devices = models.IntegerField(default=0)       #Installation cost for each devices
    sell_price_device = models.IntegerField(default=0)          #Selling price for each device

class Parameters(models.Model):
    number_of_human_day_of_work = models.IntegerField(default=0)   #Number of human day of work
    number_of_technician = models.IntegerField(default=0)          #Number of technician
    number_of_day_at_X_technician = models.IntegerField(default=0) #Number of day at X technician
    number_of_Week = models.IntegerField(default=0)                #Number of Week
    km_round_trip = models.IntegerField(default=0)                 #Km round trip
    km_cost = models.IntegerField(default=0)                       #KM cost
    toll = models.IntegerField(default=0)                          #toll
    no_of_vehicules = models.IntegerField(default=0)               #Nb of vehicules
    cost_of_road = models.IntegerField(default=0)                  #cost of road
    accomodation_per_week = models.IntegerField(default=0)         #accomodation per week
    accomodation_total = models.IntegerField(default=0)            #accomodation total
    total_cost_of_meal =  models.IntegerField(default=0)           #total cost of meal
    margin = models.IntegerField(default=0)                        #Margin














