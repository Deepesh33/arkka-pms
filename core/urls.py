from django.contrib import admin
from . import views
from django.urls import path


urlpatterns = [
    path('', views.login, name="index"),
    path('project-listing/', views.project_listing, name="project_listing"),
    path('home/', views.home, name="home"),
    path('device-listing/', views.device_listing, name="device_listing"),
    path('equipment-listing/', views.equipment_listing, name="equipment_listing"),
    path('eqSubmit/',views.eqdata, name="eqdata"),
    path('logincheck/',views.logincheck, name="logincheck"),
    path('addequipment/',views.eqdata,name="eqdata")
]