from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse

username = "d@durapid.com"
password = "1234"

def index(request):
    return render(request, 'app/index.html')

def home(request):
    return render(request, 'app/home.html')

def login(request):
    return render(request, 'app/login.html')

def project_listing(request):
    return render(request,'app/project_listing.html')

def device_listing(request):
    return render(request,'app/device_listing.html')

def equipment_listing(request):
    return render(request,'app/equipment_listing.html')

def logincheck(request):
    if request.method == "POST":
        userid= request.POST.get('email','')
        logpass= request.POST.get('pass','')
        if userid == username and logpass == password:
            return render(request,'app/equipment_listing.html')
        else:
            return render(request,'app/login.html')

def eqdata(request):
    if request.method == "POST":
        equipment_names = request.POST.get('equipment_name','')
        eq_pur_price_exc_tax = request.POST.get('unit_purchase_price','')
        equipment_desc = request.POST.get('description','')
        eq_sell_price = request.POST.get('unit_selling_price','')
        equipment_provider = request.POST.get('eq_provider','')
        eq_notes = request.POST.get('notes_eq','')
        eq_image = request.POST.get('image_eq','')
        eq_obj=eq_obj(equipment_name=equipment_names,eq_pur_price_exc_tax=eq_pur_price_exc_tax,equipment_desc=equipment_desc,eq_sell_price=eq_sell_price,equipment_provider=equipment_provider,eq_notes=eq_notes,eq_image=eq_image)
        eq_obj.save()